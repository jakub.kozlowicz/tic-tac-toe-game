# Tic Tac Toe
Tic Tac Toe game written in C++ with SFML 2.5 library. This project was made for my
university course called Designing algorithms and methods of artificial intelligence
at Wroclaw University of Science and Technology.
It implements the minmax algorithm.

![Preview](/images/preview.png)

## Requirements
- SFML 2.5 library

### How to install
1. Debian based distributions
```sh
sudo apt install libsfml-dev libsfml-window2.5 libsfml-system2.5 libsfml-network2.5 libsfml-graphics2.5
```
2. Arch based distributions
```sh
sudo pacman -S sfml
```

## How to build a game
1. Clone this repository
```sh
git clone https://gitlab.com/jakub.kozlowicz/tic-tac-toe-game.git
```
2. Make directory for build files and go to this directory
```sh
mkdir build && cd build
```

3. Generate Makefile
```sh
cmake ..
```

4. Compile program
```sh
make -j4
```

5. Run gui version 
```sh
./tictactoe-gui
```

## Usage
Game allow you to choose playing board size and number of signs in order to win.
User pick their sign and according to chosen sign will start or computer will make
first move. Always `X` sign has first move.

#### Limitations
- User can pick playing board size from 3 to 9
- Number of sign in line in order to win cannot be greater than playing board

## License
- MIT License

## Autor
- Jakub Kozłowicz