/**
 * @file gui.cpp
 * @author Jakub Kozłowicz
 * @version 0.2
 * @date 2021-06-06
 * @copyright Copyright (c) 2021
 */

#include "gui/gui.hpp"

#include <filesystem>
#include <stdexcept>

const std::filesystem::path assetsDirectoryPath{ASSETS_DIR_PATH};

Gui::Gui()
{
    constexpr auto SCREEN_WIDTH = 720;
    constexpr auto SCREEN_HEIGHT = 960;

    const sf::Vector2f BUTTON_POSITION{500, 850};
    const sf::Vector2f BUTTON_SIZE{200, 80};
    const sf::Vector2f LOGO_POSITION{0, 0};

    // Window
    window_.create(sf::VideoMode(SCREEN_WIDTH, SCREEN_HEIGHT), "Tic Tac Toe", sf::Style::Titlebar | sf::Style::Close);
    window_.setFramerateLimit(60);

    // Font
    if(!font_.loadFromFile(assetsDirectoryPath / "FredokaOne-Regular.ttf"))
        throw std::invalid_argument("Error loading font file!");
    text_.setFont(font_);

    // X sign
    if(!xTexture_.loadFromFile(assetsDirectoryPath / "x.png")) throw std::invalid_argument("Error loading x file!");
    xTexture_.setSmooth(true);
    x_.setTexture(xTexture_, true);

    // O sign
    if(!oTexture_.loadFromFile(assetsDirectoryPath / "o.png")) throw std::invalid_argument("Error loading o file!");
    oTexture_.setSmooth(true);
    o_.setTexture(oTexture_, true);

    // Field
    if(!fieldTexture_.loadFromFile(assetsDirectoryPath / "field.png"))
        throw std::invalid_argument("Error loading field file!");
    fieldTexture_.setSmooth(true);
    fieldSprite_.setTexture(fieldTexture_, true);

    if(!optionTexture_.loadFromFile(assetsDirectoryPath / "option.png"))
        throw std::invalid_argument("Error loading textbox background file!");
    optionTexture_.setSmooth(true);

    if(!winTexture_.loadFromFile(assetsDirectoryPath / "win.png"))
        throw std::invalid_argument("Error loading textbox background file!");
    optionTexture_.setSmooth(true);

    if(!looseTexture_.loadFromFile(assetsDirectoryPath / "loose.png"))
        throw std::invalid_argument("Error loading textbox background file!");
    optionTexture_.setSmooth(true);

    if(!tieTexture_.loadFromFile(assetsDirectoryPath / "tie.png"))
        throw std::invalid_argument("Error loading textbox background file!");
    optionTexture_.setSmooth(true);

    // Size textbox
    textbox1_ = Textbox(2 * fontSize_, sf::Color::White, false);
    textbox1_.setFont(font_);
    textbox1_.setLimit(true, 1);
    textbox1Bg_.setTexture(fieldTexture_, true);

    // Winning textbox
    textbox2_ = Textbox(2 * fontSize_, sf::Color::White, false);
    textbox2_.setFont(font_);
    textbox2_.setLimit(true, 1);
    textbox2Bg_.setTexture(fieldTexture_, true);

    // Sign textbox
    textbox3_ = Textbox(2 * fontSize_, sf::Color::White, false);
    textbox3_.setFont(font_);
    textbox3_.setLimit(true, 1);
    textbox3Bg_.setTexture(fieldTexture_, true);

    // New game button
    newGameButton_ = Button("New Game", BUTTON_SIZE, fontSize_, sf::Color(46, 214, 73), sf::Color::White);
    newGameButton_.setFont(font_);
    newGameButton_.setPosition(BUTTON_POSITION);

    // Start button
    startButton_ = Button("Start", BUTTON_SIZE, fontSize_, sf::Color(46, 214, 73), sf::Color::White);
    startButton_.setFont(font_);
    startButton_.setPosition(BUTTON_POSITION);

    // Logo
    if(!logoTexture_.loadFromFile(assetsDirectoryPath / "logo.png"))
        throw std::invalid_argument("Error loading logo file!");
    logoTexture_.setSmooth(true);
    logoSprite_.setTexture(logoTexture_, true);
    logoSprite_.setPosition(LOGO_POSITION);
}

void Gui::drawInitial()
{
    window_.clear(sf::Color(102, 72, 196));
    window_.draw(logoSprite_);
    newGameButton_.drawTo(window_);
    text_.setString("Graphics by Wojciech Smola");
    text_.setPosition(20, 900);
    text_.setCharacterSize(20);
    text_.setFillColor(sf::Color::White);
    window_.draw(text_);
    window_.display();
}

void Gui::drawMenu()
{
    const sf::Vector2f SIZE_POSITION{20, 260};
    const sf::Vector2f WINNING_POSITION{20, 440};
    const sf::Vector2f SIGN_POSITION{20, 620};
    float xPos = 0, yPos = 0;

    window_.clear(sf::Color(102, 72, 196));

    window_.draw(logoSprite_);

    text_.setCharacterSize(fontSize_);
    text_.setFillColor(sf::Color::White);
    optionBackground_.setTexture(optionTexture_, true);

    configureMenu(textbox1_, textbox1Bg_, optionBackground_, "CHOOSE PLAYING BOARD SIZE:", SIZE_POSITION);
    window_.draw(optionBackground_);
    window_.draw(text_);
    window_.draw(textbox1Bg_);
    textbox1_.drawTo(window_);

    configureMenu(textbox2_, textbox2Bg_, optionBackground_,
                  "CHOOSE NUMBER OF SIGNS IN\nORDER TO WIN:", WINNING_POSITION);
    window_.draw(optionBackground_);
    window_.draw(text_);
    window_.draw(textbox2Bg_);
    textbox2_.drawTo(window_);

    configureMenu(textbox3_, textbox3Bg_, optionBackground_, "CHOOSE YOUR SIGN:", SIGN_POSITION);
    window_.draw(optionBackground_);
    window_.draw(text_);
    window_.draw(textbox3Bg_);
    textbox3_.drawTo(window_);

    startButton_.drawTo(window_);

    window_.display();
}

void Gui::drawGame()
{
    window_.clear(sf::Color(51, 34, 103));

    window_.draw(logoSprite_);
    for(const auto& field : fields_) window_.draw(field);
    for(const auto& sign : signs_) window_.draw(sign);

    window_.display();
}

void Gui::drawEnd()
{
    window_.clear(sf::Color(51, 34, 103));

    window_.draw(logoSprite_);
    for(const auto& field : fields_) window_.draw(field);
    for(const auto& sign : signs_) window_.draw(sign);

    float xPos = (window_.getSize().x / 2) - (optionBackground_.getLocalBounds().width / 2);
    float yPos = (window_.getSize().y / 2) - (optionBackground_.getLocalBounds().height / 2);
    optionBackground_.setPosition(xPos, yPos);
    window_.draw(optionBackground_);

    auto position = optionBackground_.getPosition();
    xPos = position.x + (optionBackground_.getLocalBounds().width / 2) - (text_.getLocalBounds().width / 2);
    yPos = position.y + (optionBackground_.getLocalBounds().height / 2.5) - (text_.getLocalBounds().height / 2.5);
    text_.setPosition(xPos, yPos);
    text_.setCharacterSize(2 * fontSize_);

    window_.draw(text_);
    newGameButton_.drawTo(window_);
    window_.display();
}

void Gui::draw()
{

    switch(gameState_)
    {
        case 0:
        {
            drawInitial();
            break;
        }
        case 1:
        {
            drawMenu();
            break;
        }
        case 2:
        {
            drawGame();
            break;
        }
        case 3:
        {
            drawEnd();
            break;
        }
        default:
            break;
    }
}

void Gui::configureMenu(Textbox& textbox, sf::Sprite& textboxBg, sf::Sprite& background, const std::string& text,
                        const sf::Vector2f& position)
{
    const sf::Vector2f TEXT_OFFSET{40, 0};
    const sf::Vector2f TEXTBOX_OFFSET{35, 15};
    const sf::Vector2f FIELD_OFFSET{560, 30};

    text_.setString(text);
    float xPos = (position + TEXT_OFFSET).x;
    float yPos = position.y + (optionBackground_.getLocalBounds().height / 2) - (text_.getLocalBounds().height / 2);
    text_.setPosition(xPos, yPos);
    background.setPosition(position);
    textboxBg.setPosition(position + FIELD_OFFSET);
    textbox.setPosition(position + FIELD_OFFSET + TEXTBOX_OFFSET);
}

bool Gui::elementPressed(const sf::Event& event, const sf::Sprite& sprite)
{
    return sprite.getGlobalBounds().contains(float(event.mouseButton.x), float(event.mouseButton.y));
}

void Gui::increaseGameState()
{
    gameState_++;
    if(gameState_ > 3) gameState_ = 1;
}

void Gui::reset()
{
    fields_.erase(fields_.begin(), fields_.end());
    signs_.erase(signs_.begin(), signs_.end());

    if(game_ != nullptr)
    {
        delete game_;
        game_ = nullptr;
    }
    counter_ = 0;
}

void Gui::handleMouseMovement()
{
    if(newGameButton_.hoverOver(window_))
        newGameButton_.setBgColor(sf::Color(16, 165, 65));
    else
        newGameButton_.setBgColor(sf::Color(46, 214, 73));

    if(startButton_.hoverOver(window_))
        startButton_.setBgColor(sf::Color(16, 165, 65));
    else
        startButton_.setBgColor(sf::Color(46, 214, 73));
}

void Gui::handleMouseClick(sf::Event& event)
{
    if(event.mouseButton.button == sf::Mouse::Left)
    {
        if(newGameButton_.hoverOver(window_) && gameState_ == 0)
            increaseGameState();
        else if(startButton_.hoverOver(window_) && gameState_ == 1)
            increaseGameState();
        else if(newGameButton_.hoverOver(window_) && gameState_ == 3)
        {
            reset();
            increaseGameState();
        }

        if(elementPressed(event, textbox1Bg_))
            textbox1_.setSelection(true);
        else
            textbox1_.setSelection(false);

        if(elementPressed(event, textbox2Bg_))
            textbox2_.setSelection(true);
        else
            textbox2_.setSelection(false);

        if(elementPressed(event, textbox3Bg_))
            textbox3_.setSelection(true);
        else
            textbox3_.setSelection(false);

        if(gameState_ == 2)
        {
            for(int i = 0; i < fields_.size(); i++)
            {
                if(elementPressed(event, fields_[i]))
                {
                    move_.first = i / boardSize_;
                    move_.second = i % boardSize_;

                    if(game_->board_.checkMove(move_.first, move_.second, game_->human_))
                    {
                        game_->board_.addMove(move_.first, move_.second, game_->human_);
                        if(game_->human_.sign_ == 'X')
                        {
                            x_.setPosition(fields_[i].getPosition());
                            signs_.insert(signs_.begin(), x_);
                        }
                        else
                        {
                            o_.setPosition(fields_[i].getPosition());
                            signs_.insert(signs_.begin(), o_);
                        }
                        counter_++;
                    }
                }
            }
        }
    }
}

void Gui::handleTextEntered(sf::Event& event)
{
    textbox1_.typedOn(event);
    textbox2_.typedOn(event);
    textbox3_.typedOn(event);
}

void Gui::updateGame()
{
    if(gameState_ == 2)
    {
        const sf::Vector2f INITIAL_POSITION{40, 250};

        boardSize_ = std::stoi(textbox1_.getText());
        noSigns_ = std::stoi(textbox2_.getText());
        playerSign_ = std::toupper(textbox3_.getText()[0]);

        double scale = ((window_.getSize().x - 122) / boardSize_) / fieldSprite_.getLocalBounds().width;
        fieldSprite_.setScale(scale, scale);
        o_.setScale(scale, scale);
        x_.setScale(scale, scale);

        if(fields_.empty())
        {
            fields_.reserve(boardSize_ * boardSize_);
            fields_.resize(boardSize_ * boardSize_);
        }

        if(signs_.empty())
        {
            signs_.reserve(boardSize_ * boardSize_);
            signs_.resize(boardSize_ * boardSize_);
        }

        int k = 0;
        for(int i = 0; i < boardSize_; i++)
        {
            for(int j = 0; j < boardSize_; j++)
            {
                float xPos = fieldSprite_.getGlobalBounds().width + float(0.1 * fieldSprite_.getGlobalBounds().width);
                float yPos = fieldSprite_.getGlobalBounds().height + float(0.1 * fieldSprite_.getGlobalBounds().height);

                fieldSprite_.setPosition(INITIAL_POSITION + sf::Vector2f{j * xPos, i * yPos});
                fields_[k] = fieldSprite_;
                k++;
            }
        }

        if(game_ == nullptr)
        {
            game_ = new Game(boardSize_, noSigns_, playerSign_);
            if(playerSign_ == 'X')
                counter_ = 1;
            else
                counter_ = 0;
        }
        else
        {
            if(counter_ % 2 == 0)
            {
                move_ = game_->bestMove();
                if(game_->board_.checkMove(move_.first, move_.second, game_->ai_))
                {
                    game_->board_.addMove(move_.first, move_.second, game_->ai_);

                    if(game_->ai_.sign_ == 'X')
                    {
                        x_.setPosition(fields_[move_.first * boardSize_ + move_.second].getPosition());
                        signs_.insert(signs_.begin(), x_);
                    }
                    else
                    {
                        o_.setPosition(fields_[move_.first * boardSize_ + move_.second].getPosition());
                        signs_.insert(signs_.begin(), o_);
                    }
                }
                counter_++;
            }

            if(game_->board_.winning(game_->human_, game_->ai_) == game_->human_.ID_)
            {
                text_.setString("YOU WON :)");
                optionBackground_.setTexture(winTexture_, true);
                increaseGameState();
            }
            else if(game_->board_.winning(game_->human_, game_->ai_) == game_->ai_.ID_)
            {
                text_.setString("YOU LOST :(");
                optionBackground_.setTexture(looseTexture_, true);
                increaseGameState();
            }
            else if(game_->board_.winning(game_->human_, game_->ai_) == 0)
            {
                text_.setString("IT'S TIE!");
                optionBackground_.setTexture(tieTexture_, true);
                increaseGameState();
            }
        }
    }
    else if(gameState_ == 1)
        reset();
}

void Gui::execGame()
{
    while(window_.isOpen())
    {
        sf::Event event{};
        while(window_.pollEvent(event))
        {
            switch(event.type)
            {
                case sf::Event::Closed:
                    window_.close();
                    break;

                case sf::Event::MouseMoved:
                    handleMouseMovement();
                    break;

                case sf::Event::MouseButtonPressed:
                    handleMouseClick(event);
                    break;

                case sf::Event::TextEntered:
                    handleTextEntered(event);
                    break;

                default:
                    break;
            }
        }
        draw();
        updateGame();
    }
}
