/**
 * @file console.cpp
 * @author Jakub Kozłowicz
 * @version 0.1
 * @date 2021-05-25
 * @copyright Copyright (c) 2021
 */

#include "game/game.hpp"

#include <iostream>

int main(int argc, char const* argv[])
{

    std::cout << "Tic Tac Toe in console" << std::endl;

    return 0;
}
