/**
 * @file textbox.cpp
 * @author Jakub Kozłowicz
 * @version 0.2
 * @date 2021-06-06
 * @copyright Copyright (c) 2021
 */

#include "gui/textbox.hpp"

#include <stdexcept>

void Textbox::inputLogic(int characterTyped)
{
    if(characterTyped != DELETE_KEY_ && characterTyped != ENTER_KEY_ && characterTyped != ESCAPE_KEY_)
    {
        text_ << static_cast<char>(characterTyped);
    }
    else if(characterTyped == DELETE_KEY_)
    {
        if(text_.str().length() > 0)
        {
            deleteLastCharacter();
        }
    }
    textBox_.setString(text_.str() + "|");
}

void Textbox::deleteLastCharacter()
{
    std::string text = text_.str();
    std::string newText;

    for(int i = 0; i < text.length() - 1; i++)
    {
        newText += text[i];
    }
    text_.str("");
    text_ << newText;

    textBox_.setString(text_.str());
}

Textbox::Textbox(const int& size, sf::Color color, bool selection)
{
    textBox_.setCharacterSize(size);
    textBox_.setFillColor(color);
    isSelected_ = selection;
    if(isSelected_)
        textBox_.setString("|");
    else
        textBox_.setString("");
}

Textbox::Textbox(const Textbox& otherTextbox)
{
    textBox_ = otherTextbox.textBox_;
    text_.str() = otherTextbox.text_.str();
    isSelected_ = otherTextbox.isSelected_;
    hasLimit_ = otherTextbox.hasLimit_;
    limit_ = otherTextbox.limit_;
}

Textbox& Textbox::operator=(const Textbox& other)
{
    textBox_ = other.textBox_;
    text_.str() = other.text_.str();
    isSelected_ = other.isSelected_;
    hasLimit_ = other.hasLimit_;
    limit_ = other.limit_;

    return *this;
}

void Textbox::setFont(sf::Font& font)
{
    textBox_.setFont(font);
}

void Textbox::setPosition(sf::Vector2f position)
{
    textBox_.setPosition(position);
}

void Textbox::setLimit(bool hasLimit)
{
    hasLimit_ = hasLimit;
}

void Textbox::setLimit(bool hasLimit, int limit)
{
    hasLimit_ = hasLimit;
    limit_ = limit - 1;
}

void Textbox::setSelection(bool selection)
{
    isSelected_ = selection;
    if(!isSelected_)
    {
        if(text_.str().length() != 0)
        {
            std::string text = text_.str();
            std::string newText;

            for(char i : text)
            {
                newText += i;
            }
            textBox_.setString(newText);
        }
        else
        {
            textBox_.setString("");
        }
    }
    else
    {
        if(text_.str().length() != 0)
        {
            std::string text = text_.str();
            std::string newText;

            for(char i : text)
            {
                newText += i;
            }
            textBox_.setString(newText + "|");
        }
        else
        {
            textBox_.setString("|");
        }
    }
}

std::string Textbox::getText()
{
    return text_.str();
}

void Textbox::typedOn(sf::Event input)
{
    if(isSelected_)
    {
        int charTyped = input.text.unicode;
        if(charTyped < 128)
        {
            if(hasLimit_)
            {
                if(text_.str().length() <= limit_)
                {
                    inputLogic(charTyped);
                }
                else if(text_.str().length() > limit_ && charTyped == DELETE_KEY_)
                {
                    deleteLastCharacter();
                }
            }
            else
            {
                inputLogic(charTyped);
            }
        }
    }
}

void Textbox::drawTo(sf::RenderWindow& window)
{
    window.draw(textBox_);
}