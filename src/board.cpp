/**
 * @file board.cpp
 * @author Jakub Kozłowicz
 * @version 0.2
 * @date 2021-06-04
 * @copyright Copyright (c) 2021
 */

#include "engine/board.hpp"

#include <stdexcept>

Board::Board(const int& size /*= 3*/, const int& winningNumber /*= 3*/)
{
    dimension_ = size;
    winningNumber_ = winningNumber;
    matrix_ = Matrix(dimension_);
}

Board::Board(const Board& otherBoard)
{
    dimension_ = otherBoard.dimension_;
    winningNumber_ = otherBoard.winningNumber_;
    matrix_ = Matrix(otherBoard.matrix_);
}

void Board::addMove(const int& row, const int& column, const Player& player)
{
    if(matrix_(row, column) == '_')
        matrix_(row, column) = player.sign_;
    else
        throw std::invalid_argument("This field is taken!");
}

bool Board::checkMove(const int& row, const int& column, const Player& player) const
{
    if(0 <= row && row < dimension_ && 0 <= column && column < dimension_)
    {
        if(matrix_(row, column) == '_')
            return true;
        else
            return false;
    }

    return false;
}

bool Board::leftMove() const
{
    for(int row = 0; row < dimension_; row++)
    {
        for(int column = 0; column < dimension_; column++)
        {
            if(matrix_(row, column) == '_') return true;
        }
    }

    return false;
}

void Board::removeMove(const int& row, const int& column)
{
    matrix_(row, column) = '_';
}

void Board::reset()
{
    for(int row = 0; row < dimension_; row++)
    {
        for(int column = 0; column < dimension_; column++)
        {
            matrix_(row, column) = '_';
        }
    }
}

int Board::checkHorizontal(const Player& player)
{
    bool playerWins = true;

    for(int i = 0; i < dimension_; i++)
    {
        for(int j = 0; j <= (dimension_ - winningNumber_); j++)
        {
            playerWins = true;
            for(int k = j; k < (winningNumber_ + j); k++)
            {
                if(matrix_(i, k) != player.sign_)
                {
                    playerWins = false;
                    break;
                }
            }

            if(playerWins) return player.ID_;
        }
    }

    return 0;
}

int Board::checkVertical(const Player& player)
{
    bool playerWins = true;

    for(int i = 0; i < dimension_; i++)
    {
        for(int j = 0; j <= (dimension_ - winningNumber_); j++)
        {
            playerWins = true;
            for(int k = j; k < (winningNumber_ + j); k++)
            {
                if(matrix_(k, i) != player.sign_)
                {
                    playerWins = false;
                    break;
                }
            }

            if(playerWins) return player.ID_;
        }
    }

    return 0;
}

int Board::checkBackslash(const Player& player)
{
    bool playerWins = true;

    int l;
    for(int i = 0; i <= (dimension_ - winningNumber_); i++)
    {
        for(int j = 0; j <= (dimension_ - winningNumber_); j++)
        {
            playerWins = true;
            l = i;
            for(int k = j; k < (winningNumber_ + j); k++)
            {
                if(matrix_(l, k) != player.sign_)
                {
                    playerWins = false;
                    break;
                }
                l++;
            }

            if(playerWins) return player.ID_;
        }
    }

    return 0;
}

int Board::checkSlash(const Player& player)
{
    bool playerWins = true;

    int l;
    for(int i = 0; i <= (dimension_ - winningNumber_); i++)
    {
        for(int j = 0; j <= (dimension_ - winningNumber_); j++)
        {
            playerWins = true;
            l = i;
            for(int k = j + winningNumber_ - 1; k >= 0; k--)
            {
                if(matrix_(l, k) != player.sign_)
                {
                    playerWins = false;
                    break;
                }
                l++;
            }
            if(playerWins) return player.ID_;
        }
    }

    return 0;
}

int Board::winning(const Player& player1, const Player& player2)
{
    int state = 0;
    std::vector<Player> players{player1, player2};

    for(const auto& player : players)
    {
        state = checkHorizontal(player);
        if(state) return state;
    }

    for(const auto& player : players)
    {
        state = checkVertical(player);
        if(state) return state;
    }

    for(const auto& player : players)
    {
        state = checkSlash(player);
        if(state) return state;
    }

    for(const auto& player : players)
    {
        state = checkBackslash(player);
        if(state) return state;
    }

    if(leftMove()) return -1;

    return state;
}