/**
 * @file main.cpp
 * @author Jakub Kozłowicz
 * @version 0.1
 * @date 2021-06-04
 * @copyright Copyright (c) 2021
 */

#include "gui/gui.hpp"

int main(int argc, char const* argv[])
{
    Gui gui;
    gui.execGame();

    return EXIT_SUCCESS;
}