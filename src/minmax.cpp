/**
 * @file minmax.cpp
 * @author Jakub Kozłowicz
 * @version 0.2
 * @date 2021-06-04
 * @copyright Copyright (c) 2021
 */

#include "game/minmax.hpp"

#include <limits>

int minmax(Board& board, const Player& player1, const Player& player2, bool maximizing, int depth)
{
    int maxDepth[] = {0, 0, 0, 10, 5, 4, 3, 3, 3, 2};

    int winner = board.winning(player1, player2);

    if(winner == player1.ID_) return +10;

    if(winner == player2.ID_) return -10;

    if(!(board.leftMove())) return 0;

    if(depth == maxDepth[board.size()]) return 0;

    if(maximizing)
    {
        int best = std::numeric_limits<int>::min();

        for(int row = 0; row < board.size(); row++)
        {
            for(int column = 0; column < board.size(); column++)
            {
                if(board.array()(row, column) == '_')
                {
                    board.addMove(row, column, player1);
                    best = std::max(best, minmax(board, player1, player2, !maximizing, depth + 1));
                    board.removeMove(row, column);
                }
            }
        }

        return best;
    }
    else
    {
        int best = std::numeric_limits<int>::max();

        for(int row = 0; row < board.size(); row++)
        {
            for(int column = 0; column < board.size(); column++)
            {
                if(board.array()(row, column) == '_')
                {
                    board.addMove(row, column, player2);
                    best = std::min(best, minmax(board, player1, player2, !maximizing, depth + 1));
                    board.removeMove(row, column);
                }
            }
        }

        return best;
    }
}