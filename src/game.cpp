/**
 * @file game.cpp
 * @author Jakub Kozłowicz
 * @version 0.2
 * @date 2021-06-04
 * @copyright Copyright (c) 2021
 */

#include "game/game.hpp"
#include "game/minmax.hpp"

#include <limits>

Game::Game(const int& size, const int& noSigns, const char& sign)
{
    board_ = Board(size, noSigns);

    switch(sign)
    {
        case 'X':
        {
            human_ = Player('X');
            ai_ = Player('O');
            break;
        }
        case 'O':
        {
            ai_ = Player('X');
            human_ = Player('O');
            break;
        }

        default:
            break;
    }
}

std::pair<int, int> Game::bestMove()
{
    int bestScore = std::numeric_limits<int>::min();
    std::pair<int, int> bestMove{-1, -1};

    for(int row = 0; row < board_.size(); row++)
    {
        for(int column = 0; column < board_.size(); column++)
        {
            if(board_.array()(row, column) == '_')
            {
                board_.addMove(row, column, ai_);

                int minmaxValue = minmax(board_, ai_, human_, false, 0);

                board_.removeMove(row, column);

                if(minmaxValue > bestScore)
                {
                    bestMove.first = row;
                    bestMove.second = column;
                    bestScore = minmaxValue;
                }
            }
        }
    }

    return bestMove;
}