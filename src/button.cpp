/**
 * @file button.cpp
 * @author Jakub Kozłowicz
 * @version 0.2
 * @date 2021-06-06
 * @copyright Copyright (c) 2021
 */

#include "gui/button.hpp"

Button::Button(const std::string& text, sf::Vector2f size, int charSize, sf::Color bgColor, sf::Color textColor)
{
    text_.setString(text);
    text_.setFillColor(textColor);
    text_.setCharacterSize(charSize);

    button_.setSize(size);
    button_.setFillColor(bgColor);
}

void Button::setFont(sf::Font& font)
{
    text_.setFont(font);
}

void Button::setBgColor(sf::Color color)
{
    button_.setFillColor(color);
}

void Button::setTextColor(sf::Color color)
{
    text_.setFillColor(color);
}

void Button::setPosition(sf::Vector2f position)
{
    button_.setPosition(position);

    float xPos = (position.x + button_.getLocalBounds().width / 2) - (text_.getLocalBounds().width / 2);
    float yPos = (position.y + button_.getLocalBounds().height / 2.5) - (text_.getLocalBounds().height / 2.5);

    text_.setPosition(sf::Vector2f{xPos, yPos});
}

bool Button::hoverOver(sf::RenderWindow& window)
{
    float xMouse = float(sf::Mouse::getPosition(window).x);
    float yMouse = float(sf::Mouse::getPosition(window).y);

    float btnPosX = button_.getPosition().x;
    float btnPosY = button_.getPosition().y;

    float xBtnPosWidth = button_.getPosition().x + button_.getLocalBounds().width;
    float yBtnPosHeight = button_.getPosition().y + button_.getLocalBounds().height;

    if(xMouse < xBtnPosWidth && xMouse > btnPosX && yMouse < yBtnPosHeight && yMouse > btnPosY)
        return true;
    else
        return false;
}

void Button::drawTo(sf::RenderWindow& window)
{
    window.draw(button_);
    window.draw(text_);
}
