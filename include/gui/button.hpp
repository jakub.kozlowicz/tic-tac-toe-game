/**
 * @file button.hpp
 * @author Jakub Kozłowicz
 * @version 0.2
 * @date 2021-06-04
 * @copyright Copyright (c) 2021
 */

#ifndef BUTTON_HPP_
#define BUTTON_HPP_

#include <SFML/Graphics.hpp>

#include <string>

class Button
{
  private:
    sf::RectangleShape button_; /** @brief Rectangle background of the button */
    sf::Text text_;             /** @brief Text on the button */

  public:
    /**
     * @brief Construct a new Button object
     */
    Button() = default;

    /**
     * @brief Construct a new Button object
     * @param text Text on the button
     * @param size Size of the button
     * @param charSize Size of the text
     * @param bgColor Color of the button
     * @param textColor Color of the text
     */
    Button(const std::string& text, sf::Vector2f size, int charSize, sf::Color bgColor, sf::Color textColor);

    /**
     * @brief Set font of the text
     * @param font Font type
     */
    void setFont(sf::Font& font);

    /**
     * @brief Set button color
     * @param color Button color
     */
    void setBgColor(sf::Color color);

    /**
     * @brief Set text color
     * @param color Text color
     */
    void setTextColor(sf::Color color);

    /**
     * @brief Set position of the button
     * @param position Vector of the position
     */
    void setPosition(sf::Vector2f position);

    /**
     * @brief Check if mouse is over button
     * @param window Reference to window
     * @return Boolean value if mouse is over button
     * @retval true - mouse is over button
     * @retval false - mouse is not over button
     */
    bool hoverOver(sf::RenderWindow& window);

    /**
     * @brief Draw button to window
     * @param window Reference to window
     */
    void drawTo(sf::RenderWindow& window);

    /**
     * @brief Destroy the Button object
     */
    ~Button() = default;
};

#endif /* BUTTON_HPP_ */