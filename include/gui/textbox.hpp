/**
 * @file textbox.hpp
 * @author Jakub Kozłowicz
 * @version 0.2
 * @date 2021-06-04
 * @copyright Copyright (c) 2021
 */

#ifndef TEXTBOX_HPP_
#define TEXTBOX_HPP_

#include <SFML/Graphics.hpp>

#include <sstream>
#include <string>

class Textbox
{
  private:
    const int DELETE_KEY_ = 8;  /** @brief Unicode value of delete key */
    const int ENTER_KEY_ = 13;  /** @brief Unicode value of return key */
    const int ESCAPE_KEY_ = 27; /** @brief Unicode value of escape key */

    sf::Text textBox_;        /** @brief Text in textbox */
    std::ostringstream text_; /** @brief String stream to manipulate text in textbox */
    bool isSelected_{false};  /** @brief Boolean value if the textbox is selected */
    bool hasLimit_{false};    /** @brief Boolean value if textbox has a limit */
    int limit_{};             /** @brief Number of characters limit */

    /**
     * @brief Whole input to textbox logic
     * @param characterTyped Character typed in event
     */
    void inputLogic(int characterTyped);

    /**
     * @brief Delete last character from textbox
     */
    void deleteLastCharacter();

  public:
    /**
     * @brief Construct a new Textbox object
     */
    Textbox() = default;

    /**
     * @brief Construct a new Textbox object
     * @param size Character size in textbox
     * @param color Color of the text in textbox
     * @param selection ToF value of selecting textbox
     */
    Textbox(const int& size, sf::Color color, bool selection);

    /**
     * @brief Construct a new Textbox object
     * @param otherTextbox Other Textbox class to copy
     */
    Textbox(const Textbox& otherTextbox);

    /**
     * @brief Assign whole class to other
     * @param other Other Textbox class
     * @return Reference to Textbox class
     */
    Textbox& operator=(const Textbox& other);

    /**
     * @brief Set font of the textbox
     * @param font Font type
     */
    void setFont(sf::Font& font);

    /**
     * @brief Set position of the textbox
     * @param position Vector of position
     */
    void setPosition(sf::Vector2f position);

    /**
     * @brief Set value if textbox has limit
     * @param hasLimit Boolean value of textbox limit
     */
    void setLimit(bool hasLimit);

    /**
     * @brief Set if textbox has limit with value of the limit
     * @param hasLimit Boolean value of textbox limit
     * @param limit Value of textbox's limit
     */
    void setLimit(bool hasLimit, int limit);

    /**
     * @brief Set if textbox is selected
     * @param selection Boolean value if textbox is selected
     */
    void setSelection(bool selection);

    /**
     * @brief Get string from textbox
     * @return String from textbox
     */
    std::string getText();

    /**
     * @brief Handle typing event
     * @param input Event from window
     */
    void typedOn(sf::Event input);

    /**
     * @brief Draw textbox to window
     * @param window Reference to window
     */
    void drawTo(sf::RenderWindow& window);

    /**
     * @brief Destroy the Textbox object
     */
    ~Textbox() = default;
};

#endif /* TEXTBOX_HPP_ */