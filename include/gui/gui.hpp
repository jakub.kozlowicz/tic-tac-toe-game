/**
 * @file gui.hpp
 * @author Jakub Kozłowicz
 * @version 0.2
 * @date 2021-06-06
 * @copyright Copyright (c) 2021
 */

#ifndef GUI_HPP_
#define GUI_HPP_

#include "game/game.hpp"
#include "gui/button.hpp"
#include "gui/textbox.hpp"

#include <SFML/Graphics.hpp>
#include <vector>

/**
 * @brief Class representing gui to tic tac toe game
 */
class Gui
{
  private:
    sf::RenderWindow window_; /** @brief Gui window */

    int fontSize_{30}; /** @brief Font size */
    sf::Font font_;    /** @brief Font containing handle to file */
    sf::Text text_;    /** @brief Text displayed on the screen */

    sf::Texture optionTexture_;   /** @brief Texture with option background */
    sf::Texture winTexture_;      /** @brief Texture with winning sign background */
    sf::Texture looseTexture_;    /** @brief Texture with loosing sign background */
    sf::Texture tieTexture_;      /** @brief Texture with draw sign background */
    sf::Sprite optionBackground_; /** @brief Background to option in menu */

    Textbox textbox1_;      /** @brief Textbox used in menu to get board size */
    sf::Sprite textbox1Bg_; /** @brief Background to size textbox */

    Textbox textbox2_;      /** @brief Textbox used in menu to get number of signs in order to win */
    sf::Sprite textbox2Bg_; /** @brief Background to winning number textbox */

    Textbox textbox3_;      /** @brief Textbox used in menu to get player sign */
    sf::Sprite textbox3Bg_; /** @brief Background to sign textbox */

    Button newGameButton_; /** @brief Button with New Game sign */
    Button startButton_;   /** @brief Button with Start sign */

    sf::Texture xTexture_; /** @brief Texture with "X" sign */
    sf::Sprite x_;         /** @brief "X" sign */

    sf::Texture oTexture_; /** @brief Texture with "O" sign */
    sf::Sprite o_;         /** @brief "O" sign */

    sf::Texture logoTexture_; /** @brief Texture containing game logo */
    sf::Sprite logoSprite_;   /** @brief Sprite with logo texture */

    sf::Texture fieldTexture_; /** @brief Texture containing field image */
    sf::Sprite fieldSprite_;   /** @brief Sprite with field texture */

    std::vector<sf::Sprite> fields_; /** @brief std::vector containing fields sprites */
    std::vector<sf::Sprite> signs_;  /** @brief std::vector containing signs sprites */

    int gameState_{0};         /** @brief State of the game (Initial state is set to 0) */
    int boardSize_{0};         /** @brief Size of the playing board */
    int noSigns_{0};           /** @brief Number of signs in order to win*/
    char playerSign_{'X'};     /** @brief Player's sign */
    Game* game_{nullptr};      /** @brief Pointer to game class */
    int counter_{0};           /** @brief Game move counter to match player's move */
    std::pair<int, int> move_; /** @brief Structure containing movement coordinates */

    /**
     * @brief Draw initial state of the game
     */
    void drawInitial();

    /**
     * @brief Draw game menu
     */
    void drawMenu();

    /**
     * @brief Draw game board and current state of the game
     */
    void drawGame();

    /**
     * @brief Draw end screen of the game
     */
    void drawEnd();

    /**
     * @brief Draw every state of game to window
     */
    void draw();

    /**
     * @brief Set appropriate positions to elements in menu
     * @param textbox Textbox
     * @param textboxBg Textbox background
     * @param background Option background
     * @param text Option text
     * @param position Main position (top left corner)
     */
    void configureMenu(Textbox& textbox, sf::Sprite& textboxBg, sf::Sprite& background, const std::string& text,
                       const sf::Vector2f& position);

    /**
     * @brief Check if element given as parameter has been pressed
     * @param event Window event
     * @param sprite Element to check
     * @return Boolean value if element has been pressed
     * @retval true - Element has been pressed
     * @retval false - Element has not been pressed
     */
    bool elementPressed(const sf::Event& event, const sf::Sprite& sprite);

    /**
     * @brief Increase game state
     */
    void increaseGameState();

    /**
     * @brief Reset whole game
     */
    void reset();

    /**
     * @brief Handle mouse movement
     */
    void handleMouseMovement();

    /**
     * @brief Handle mouse pressed button
     * @param event Window event
     */
    void handleMouseClick(sf::Event& event);

    /**
     * @brief Handle text entered
     * @param event Window event
     */
    void handleTextEntered(sf::Event& event);

    /**
     * @brief Update game execution
     * @details Handle position of fields and sign. Also make
     * moves for AI and connect gui with game engine.
     */
    void updateGame();

  public:
    /**
     * @brief Construct a new Gui object
     */
    Gui();

    /**
     * @brief Execute game with gui
     */
    void execGame();

    /**
     * @brief Destroy the Gui object
     */
    ~Gui() = default;
};

#endif /* GUI_HPP_ */