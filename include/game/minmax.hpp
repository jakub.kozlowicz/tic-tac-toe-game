/**
 * @file minmax.hpp
 * @author Jakub Kozłowicz
 * @version 0.2
 * @date 2021-06-04
 * @copyright Copyright (c) 2021
 */

#ifndef MINMAX_HPP_
#define MINMAX_HPP_

#include "engine/board.hpp"
#include "engine/player.hpp"

/**
 * @brief Algorithm which help with finding best possible move
 * @param board Playing board
 * @param player1 First player
 * @param player2 Second player
 * @param maximizing Boolean value if algorithm should be maximizing or minimizing values
 * @param depth Depth when algorithm ends it's recursion
 * @return Value of maximizing or minimizing
 */
int minmax(Board& board, const Player& player1, const Player& player2, bool maximizing, int depth);

#endif /* MINMAX_HPP_ */