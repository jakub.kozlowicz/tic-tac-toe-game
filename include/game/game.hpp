/**
 * @file game.hpp
 * @author Jakub Kozłowicz
 * @version 0.2
 * @date 2021-06-04
 * @copyright Copyright (c) 2021
 */

#include "engine/board.hpp"
#include "engine/player.hpp"
#include "minmax.hpp"
/**
 * @brief Class representing game logic
 */
class Game
{
  public:
    Board board_;  /** @brief Playing board */
    Player ai_;    /** @brief AI player */
    Player human_; /** @brief Human player */

    /**
     * @brief Construct a new Game object
     * @param size Size of the playing board
     * @param noSigns Number of signs in order to win
     * @param sign Human player sign
     */
    Game(const int& size, const int& noSigns, const char& sign);

    /**
     * @brief Find best move with minmax algorithm
     * @return Coordinates of best possible move
     */
    std::pair<int, int> bestMove();

    /**
     * @brief Destroy the Game object
     */
    ~Game() = default;
};
