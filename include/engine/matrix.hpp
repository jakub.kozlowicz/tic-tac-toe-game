/**
 * @file matrix.hpp
 * @author Jakub Kozłowicz
 * @version 0.2
 * @date 2021-06-04
 * @copyright Copyright (c) 2021
 */

#ifndef MATRIX_HPP_
#define MATRIX_HPP_

#include <vector>

/**
 * @brief Structure representing two dimensional array
 */
struct Matrix
{
    std::vector<char> matrix_; /** @brief 2D array containing players signs */
    int dimension_;            /** @brief Size of the 2D array */

    /**
     * @brief Construct a new Matrix object
     * @details Resize vector size to appropriate dimensions.
     * @param size Size of two dimensional array
     */
    explicit Matrix(const int& size = 1)
    {
        dimension_ = size;
        matrix_.resize(dimension_ * dimension_, '_');
    }

    /**
     * @brief Construct a new Matrix object
     * @param otherMatrix Another Matrix structure to copy
     */
    Matrix(const Matrix& otherMatrix) : Matrix(otherMatrix.dimension_) { matrix_ = otherMatrix.matrix_; }

    /**
     * @brief Return reference to appropriate cell
     * @param row number of row
     * @param column number of column
     * @return Reference to appropriate cell
     */
    char& operator()(const int& row, const int& column) { return matrix_[row * dimension_ + column]; }

    /**
     * @brief Return value of appropriate cell
     * @param row number of row
     * @param column number of column
     * @return Value of the cell
     */
    const char& operator()(const int& row, const int& column) const { return matrix_[row * dimension_ + column]; }

    /**
     * @brief Return size of the 2D array
     * @return Size of the 2D array
     */
    [[nodiscard]] int size() const { return dimension_; }

    /**
     * @brief Destroy the Matrix object
     */
    ~Matrix() = default;
};

#endif /* MATRIX_HPP_ */