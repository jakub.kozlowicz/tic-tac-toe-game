/**
 * @file board.hpp
 * @author Jakub Kozłowicz
 * @version 0.2
 * @date 2021-06-04
 * @copyright Copyright (c) 2021
 */

#ifndef BOARD_HPP_
#define BOARD_HPP_

#include "matrix.hpp"
#include "player.hpp"

/**
 * @brief Class representing tic tac toe board
 */
class Board
{
  private:
    int dimension_;     /** @brief Size of the playing board */
    Matrix matrix_;     /** @brief 2D array containing players signs */
    int winningNumber_; /** @brief Number of signs in order to win */

    /**
     * @brief Check rows to verify if player win
     * @param player Reference to player structure
     * @return Player ID if player win or 0 if it's tie
     */
    int checkHorizontal(const Player& player);

    /**
     * @brief Check columns if player win
     * @param player Reference to player structure
     * @return Player ID if player win or 0 if it's tie
     */
    int checkVertical(const Player& player);

    /**
     * @brief Check slash if player win
     * @param player Reference to player structure
     * @return Player ID if player win or 0 if it's tie
     */
    int checkSlash(const Player& player);

    /**
     * @brief Check backslash if player win
     * @param player Reference to player structure
     * @return Player ID if player win or 0 if it's tie
     */
    int checkBackslash(const Player& player);

  public:
    /**
     * @brief Construct a new Board object
     * @param size Size of the board
     * @param winningNumber Number of sings in order to win
     */
    explicit Board(const int& size = 3, const int& winningNumber = 3);

    /**
     * @brief Construct a new Board object
     * @param otherBoard Other instance of the Board class to copy
     */
    Board(const Board& otherBoard);

    /**
     * @brief Add player's move to the board
     * @param row row index
     * @param column column index
     * @param player player type
     */
    void addMove(const int& row, const int& column, const Player& player);

    /**
     * @brief Return 2D array with fields
     * @return Reference to two dimensional array containing players's signs
     */
    Matrix& array() { return matrix_; }

    /**
     * @brief Check if move is possible
     * @param row row index
     * @param column column index
     * @param player player type
     * @return State if the move is possible
     * @retval true - The move is possible for this player
     * @retval false - The move is impossible for this player
     */
    [[nodiscard]] bool checkMove(const int& row, const int& column, const Player& player) const;

    /**
     * @brief Check if any possible move left
     * @retval true - There is possible move
     * @retval false - There is not any possible move
     */
    [[nodiscard]] bool leftMove() const;

    /**
     * @brief Remove player's move
     * @param row row index
     * @param column column index
     */
    void removeMove(const int& row, const int& column);

    /**
     * @brief Reset the board
     */
    void reset();

    /**
     * @brief Return size of the board
     * @return Size of the board
     */
    [[nodiscard]] int size() const { return dimension_; }

    /**
     * @brief Check if one of players win or it is tie
     * @param player1 first player
     * @param player2 second player
     * @return State of the game
     * @retval [1] - Player with ID = 1 wins
     * @retval [2] - Player with ID = 2 wins
     * @retval [0] - It's tie
     * @retval [-1] - Neither of players win
     */
    [[nodiscard]] int winning(const Player& player1, const Player& player2);

    /**
     * @brief Destroy the Board object
     */
    ~Board() = default;
};

#endif /* BOARD_HPP_ */