/**
 * @file player.hpp
 * @author Jakub Kozłowicz
 * @version 0.2
 * @date 2021-06-04
 * @copyright Copyright (c) 2021
 */

#ifndef PLAYER_HPP_
#define PLAYER_HPP_

/**
 * @brief Structure representing computer or human player
 */
struct Player
{
    int ID_;    /** @brief Type of the player */
    char sign_; /** @brief Player's sign - "X" or "O" */

    /**
     * @brief Construct a new Player object
     * @param playerSign Player's sign
     */
    explicit Player(char playerSign = 'X') : sign_(playerSign)
    {
        switch(playerSign)
        {
            case 'X':
            {
                ID_ = 1;
                break;
            }
            case 'O':
            {
                ID_ = 2;
                break;
            }
            default:
                break;
        }
    }
};

#endif /* PLAYER_HPP_ */